package cl.prueba.api.coe.dao;

import cl.prueba.api.coe.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UsuarioDao extends JpaRepository<Usuario, Integer> {
    List<Usuario> findByCorreo(String correo);
}
