package cl.prueba.api.coe.advice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CustomControllerAdvice extends Exception {

    private static final long serialVersionUID = 7649300433965570965L;

    @ExceptionHandler(PlataformaException.class)
    public ResponseEntity<MessageError> handleCustomException(PlataformaException ex) {
        MessageError me = new MessageError(ex.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(me);
    }
}

