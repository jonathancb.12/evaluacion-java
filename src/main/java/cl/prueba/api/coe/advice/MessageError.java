package cl.prueba.api.coe.advice;

public class MessageError {

    private String mensaje;

    public MessageError(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
