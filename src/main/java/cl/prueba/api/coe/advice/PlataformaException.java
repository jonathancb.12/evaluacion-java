package cl.prueba.api.coe.advice;

public class PlataformaException extends RuntimeException {

    public PlataformaException(String message) {
        super(message);
    }
}
