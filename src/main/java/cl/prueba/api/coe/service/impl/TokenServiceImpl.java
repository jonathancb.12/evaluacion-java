package cl.prueba.api.coe.service.impl;

import cl.prueba.api.coe.service.TokenService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Date;
import java.util.UUID;

@Service
public class TokenServiceImpl implements TokenService {

    private static final String SUBJECT = "evaluacion";

    private final Key key;

    public TokenServiceImpl() {
        key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
    }

    @Override
    public String generarToken(String correo) {

        return Jwts.builder()
                .setId(UUID.randomUUID().toString())
                .setSubject(SUBJECT)
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + 7200000)) //Duración 2 horas
                .signWith(key)
                .compact();
    }
}