package cl.prueba.api.coe.service;

import cl.prueba.api.coe.entity.Usuario;

import java.util.List;

public interface UsuarioService {

    Usuario registrarUsuario(Usuario user) throws Exception;

    List<Usuario> listarUsuarios();
}
