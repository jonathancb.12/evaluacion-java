package cl.prueba.api.coe.service.impl;

import cl.prueba.api.coe.dao.UsuarioDao;
import cl.prueba.api.coe.entity.Usuario;
import cl.prueba.api.coe.service.TokenService;
import cl.prueba.api.coe.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static cl.prueba.api.coe.constants.Constantes.CORREO_EXISTENTE;
import static cl.prueba.api.coe.constants.Constantes.FORMATO_CORREO;
import static cl.prueba.api.coe.constants.Constantes.FORMATO_PASS;
import static cl.prueba.api.coe.constants.Constantes.REGEX_CORREO;
import static cl.prueba.api.coe.constants.Constantes.REGEX_PASS;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private TokenService tokenService;

    @Autowired
    private UsuarioDao usuarioDao;

    @Override
    public Usuario registrarUsuario(Usuario user) throws Exception {
        verificarUsuario(user);
        user.setToken(tokenService.generarToken(user.getCorreo()));
        return usuarioDao.save(user);
    }

    @Override
    public List<Usuario> listarUsuarios() {
        return usuarioDao.findAll();
    }

    private void verificarUsuario(Usuario user) throws Exception {
        if (!usuarioDao.findByCorreo(user.getCorreo()).isEmpty()) {
            throw new Exception(CORREO_EXISTENTE);
        }

        Pattern pattern = Pattern.compile(REGEX_CORREO);
        Matcher matcher = pattern.matcher(user.getCorreo());
        if (!matcher.matches()) {
            throw new Exception(FORMATO_CORREO);
        }

        pattern = Pattern.compile(REGEX_PASS);
        matcher = pattern.matcher(user.getContrasena());
        if (!matcher.matches()) {
            throw new Exception(FORMATO_PASS);
        }
    }
}
