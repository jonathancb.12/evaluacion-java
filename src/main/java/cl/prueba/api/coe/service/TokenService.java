package cl.prueba.api.coe.service;

public interface TokenService {

    String generarToken(String correo);
}
