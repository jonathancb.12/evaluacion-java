package cl.prueba.api.coe.constants;

public class Constantes {

    public Constantes() {
    }

    public static final String CORREO_EXISTENTE = "El correo ya se encuentra registrado.";
    public static final String FORMATO_CORREO = "El correo no cuenta con el formato correspondiente.";
    public static final String FORMATO_PASS = "La contraseña no cuenta con el formato correspondiente.";
    public static final String REGEX_CORREO = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$";
    public static final String REGEX_PASS = "^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9].*[0-9]).*$";
}
