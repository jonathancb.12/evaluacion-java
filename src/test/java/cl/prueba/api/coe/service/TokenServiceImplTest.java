package cl.prueba.api.coe.service;

import cl.prueba.api.coe.service.impl.TokenServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

public class TokenServiceImplTest {

    @InjectMocks
    TokenServiceImpl tokenService;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void generarToken() {
        Assert.assertNotNull(tokenService.generarToken(""));
    }
}
