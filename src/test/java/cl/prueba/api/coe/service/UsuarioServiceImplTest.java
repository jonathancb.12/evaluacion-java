package cl.prueba.api.coe.service;

import cl.prueba.api.coe.dao.UsuarioDao;
import cl.prueba.api.coe.entity.Usuario;
import cl.prueba.api.coe.fixture.UsuarioFixture;
import cl.prueba.api.coe.service.impl.UsuarioServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class UsuarioServiceImplTest {

    @InjectMocks
    private UsuarioServiceImpl usuarioService;

    @Mock
    private UsuarioDao usuarioDao;

    @Mock
    private TokenService tokenService;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void registrarUsuarioTest() throws Exception {
        when(usuarioDao.save(any(Usuario.class))).thenReturn(UsuarioFixture.getUsuario());
        Assert.assertNotNull(usuarioService.registrarUsuario(UsuarioFixture.getUsuario()));
    }

    @Test
    public void listarUsuariosTest() throws Exception {
        when(usuarioDao.findAll()).thenReturn(UsuarioFixture.getListUsuario());
        Assert.assertNotEquals(Collections.EMPTY_LIST, usuarioService.listarUsuarios());
    }

    @Test(expected = Exception.class)
    public void registrarUsuarioErrorCorreoTest() throws Exception {
        Usuario u = UsuarioFixture.getUsuario();
        u.setCorreo("");
        Assert.assertNotNull(usuarioService.registrarUsuario(u));
    }

    @Test(expected = Exception.class)
    public void registrarUsuarioErrorPassTest() throws Exception {
        Usuario u = UsuarioFixture.getUsuario();
        u.setContrasena("");
        Assert.assertNotNull(usuarioService.registrarUsuario(u));
    }

    @Test(expected = Exception.class)
    public void registrarUsuarioDuplicadoErorTest() throws Exception {
        when(usuarioDao.findByCorreo(any(String.class))).thenReturn(UsuarioFixture.getListUsuario());
        Assert.assertNotNull(usuarioService.registrarUsuario(UsuarioFixture.getUsuario()));
    }
}
