package cl.prueba.api.coe.controller;

import cl.prueba.api.coe.advice.PlataformaException;
import cl.prueba.api.coe.dao.UsuarioDao;
import cl.prueba.api.coe.entity.Usuario;
import cl.prueba.api.coe.fixture.UsuarioFixture;
import cl.prueba.api.coe.service.UsuarioService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class UsuarioControllerTest {

    @InjectMocks
    private UsuarioController usuarioController;

    @Mock
    private UsuarioService usuarioService;

    @Mock
    private UsuarioDao usuarioDao;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void registrarUsuario() {
        when(usuarioDao.save(any(Usuario.class))).thenReturn(UsuarioFixture.getUsuario());
        Assert.assertNotNull(usuarioController.registrarUsuario(UsuarioFixture.getUsuario()));
    }

    @Test
    public void listarUsuarios() {
        when(usuarioDao.findAll()).thenReturn(UsuarioFixture.getListUsuario());
        Assert.assertNotEquals(Collections.EMPTY_LIST, usuarioController.listarUsuario());
    }

    @Test(expected = PlataformaException.class)
    public void registrarUsuarioErrorTest() throws Exception {
        when(usuarioService.registrarUsuario(any(Usuario.class))).thenThrow(new PlataformaException(""));
        usuarioController.registrarUsuario(new Usuario());
    }
}