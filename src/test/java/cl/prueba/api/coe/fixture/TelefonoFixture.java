package cl.prueba.api.coe.fixture;

import cl.prueba.api.coe.entity.Telefono;

import java.util.Collections;
import java.util.List;

public class TelefonoFixture {

    public static Telefono getTelefono() {
        Telefono t = new Telefono();
        t.setNumero("12323");
        t.setCodCiudad("9");
        t.setCodPais("54");
        return t;
    }

    public static List<Telefono> getListaTelefono() {
        return Collections.singletonList(getTelefono());
    }
}
