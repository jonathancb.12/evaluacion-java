package cl.prueba.api.coe.fixture;

import cl.prueba.api.coe.entity.Usuario;

import java.util.Collections;
import java.util.Date;
import java.util.List;

public class UsuarioFixture {

    public static Usuario getUsuario() {
        Usuario u = new Usuario();
        u.setId("2262f957-8998-4607-8bd3-f6b40233dfd5");
        u.setCreated(new Date());
        u.setModified(new Date());
        u.setLastLogin(new Date());
        u.setToken("eyJhbGciOiJIUzI1NiJ9");
        u.setActive(true);
        u.setNombre("Juan");
        u.setCorreo("juan@dominio.cl");
        u.setContrasena("1H2Sql");
        u.setTelefonos(TelefonoFixture.getListaTelefono());
        return u;
    }

    public static List<Usuario> getListUsuario() {
        return Collections.singletonList(getUsuario());
    }
}
